#!/usr/bin/env zsh
let pagenum=0
for file in "$@"
do
    let npages=$(mdls -name kMDItemNumberOfPages $file | sed 's/kMDItemNumberOfPages = //')
    let pagenum=$pagenum+$npages
done
echo $pagenum
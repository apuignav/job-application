#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# @file   createbbl.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   26.01.2016
# =============================================================================
"""Create suitable bbl file from all bibliography files."""


import logging
import os

import argparse

import bibtexparser


logger = logging.getLogger('bibtexparser.bparser')
logger.addHandler(logging.StreamHandler())


ABBREVIATIONS = {'JHEP'                   : 'JHEP',
                 'Nucl. Instrum. Meth.'   : 'Nucl. Instrum. Meth.',
                 'Int. J. Mod. Phys.'     : 'Int. J. Mod. Phys.',
                 'JINST'                  : 'JINST',
                 'New J. Phys.'           : 'New J. Phys.',
                 'Nature'                 : 'Nature',
                 'PoS'                    : 'PoS',
                 'Phys. Rev. Lett.'       : 'Phys. Rev. Lett.',
                 'Nature Physics'         : 'Nature Physics',
                 'IEEE Trans. Nucl. Sci.' : 'IEEE Trans. Nucl. Sci.',
                 'J. Phys.'               : 'J. Phys.',
                 'Nucl. Phys.'            : 'Nucl. Phys.',
                 'Chin. Phys. C'          : 'Chin. Phys. C',
                 'Phys. Rev.'             : 'Phys. Rev.',
                 'Phys. Lett.'            : 'Phys. Lett.',
                 'J. Phys. Conf. Ser.'    : 'J. Phys. Conf. Ser.',
                 'Eur. Phys. J.'          : 'Eur. Phys. J.',
                 'Comput. Phys. Commun.'  : 'Comput. Phys. Commun.',
                 'CERN Proceedings'       : 'CERN Proc.',
                 'Nuclear Science Symposium Conference Record\n(NSS/MIC)': 'NSS/MIC',
                 'IEEE Trans.Nucl.Sci.'   : 'IEEE Trans. Nucl. Sci.'}


def load_bibtex(bib_file):
    """Load Bibtex database.

    Abbreviations are applied and LHCb commands removed.

    Arguments:
        bib_file (str): bib file to load.

    Return:
        bibtexparser.bibdatabase.BibDatabase: Loaded database

    Raise:
        KeyError: If the journal is not recognized

    """
    def customizations(record):
        """Apply customizations to loaded records."""
        if 'journal' in record:
            new_journal = ABBREVIATIONS.get(record['journal'], record['journal'])
            if not new_journal:
                logging.exception("Cannot find abbreviation for journal %s", record['journal'])
                raise Exception
            record['journal'] = new_journal
        if 'title' in record:
            if r'\lhcb' in record:
                record['title'] = record['title'].replace(r'\lhcb', 'LHCb')
        return record

    if not os.path.exists(bib_file):
        raise OSError("Cannot find file %s" % bib_file)
    with open(bib_file) as bibtex_file:
        parser = bibtexparser.bparser.BibTexParser(common_strings=True)
        parser.ignore_nonstandard_types = False
        parser.customization = customizations
        bib_database = bibtexparser.load(bibtex_file, parser=parser)
    return bib_database


def format_entry(record, title_in_output):
    """Format the record to output.

    Arguments:
        record: BibTexParser record.
        title_in_output (bool): Include title in output record?

    Return:
        str: What to print.

    Raise:
        KeyError: If cannot format entry.

    """
    def process_pages(pages):
        """Add proper double hyphen in page specification.

        Arguments:
            pages (str): Page specification.

        Returns:
            str: Updated page specification

        """
        if '--' not in pages:
            pages = pages.replace('-', '--')
        return pages.replace(' -', '-').replace('- ', '-')

    entry_type = record['ENTRYTYPE']
    output_str = "``%s'', " % record['title'] if title_in_output else ''
    eprint = False
    try:
        if entry_type == 'article':
            if not 'journal' in record:
                if 'eprint' in record or \
                        'note' in record and (
                            'submitted' in record['note'] or
                            'to appear' in record['note']):
                    eprint = True
                    output_str += '%s:%s' % (record['archiveprefix'], record['eprint'])
                elif 'number' in record:
                    output_str += record['number']
                else:
                    raise KeyError("No journal info")
            else:
                output_str += record['journal']
            if 'volume' in record:
                output_str += ' %s' % record['volume']
            if not 'year' in record:
                raise KeyError("Missing year")
            output_str += ' (%s)' % record['year']
            if not eprint:
                if not 'pages' in record:
                    output_str += ' doi:%s (%s)' % (record['doi'],
                                                  record['year'])
                else:
                    output_str += ' ' + process_pages(record['pages'])
            if 'doi' in record:
                output_str = '\href{{{}}}{{{}}}'.format('https://doi.org/{}'.format(record['doi']),
                                                        output_str)
            elif 'eprint' in record and record['archiveprefix'].lower() == 'arxiv':
                output_str = '\href{{{}}}{{{}}}'.format('https://arxiv.org/abs/{}'.format(record['eprint']),
                                                        output_str)
            elif 'number' in record:
                number = record['number'].split()[0]
        elif entry_type in ('report', 'techreport'):
            if 'number' in record:
                number = record['number'].split()[0].rstrip('.')
                output_str += '\href{{{}}}{{{}}}'.format('https://cds.cern.ch/search?ln=en&p1={}&f1=reportnumber'.format(number),
                                                         number)
            else:
                output_str += record['ID']

        elif entry_type == 'mastersthesis':
            output_str += '%s MSc thesis (%s)' % (record['author'], record['year'])
        elif entry_type == 'phdthesis':
            if 'reportnumber' in record:
                output_str += record['reportnumber']
            else:
                output_str += '%s PhD thesis' % record['author']
            output_str += ' (%s)' % record['year']
        elif entry_type == 'misc':
            if 'howpublished' in record:
                output_str += record['howpublished']
            elif 'doi' in record:
                output_str += '\href{{{}}}{{doi:{}}}'.format('https://doi.org/{}'.format(record['doi']),
                                                             record['doi'])
            elif 'ID' in record:
                output_str += record['ID']
        elif entry_type == 'inproceedings':
            if 'doi' in record:
                output_str += 'doi:%s' % record['doi']
            elif 'isbn' in record:
                output_str += 'ISBN:%s' % record['isbn']
            else:
                try:
                    output_str += record.get('shorttitle', record['booktitle'])
                except KeyError:
                    pass
            if 'number' in record:
                output_str += ' %s' % record['number']
            output_str += ' (%s)' % record['year']
            if 'pages' in record:
                output_str += ' ' + process_pages(record['pages'])
        elif entry_type == 'book':
            output_str += ' %s (%s)' % (record['title'], record['year'])
        if not output_str:
            raise KeyError("Empty output string")
    except KeyError as error:
        print(record)
        raise KeyError("Cannot format entry %s -> %s" % (record['ID'], error))
    return output_str


def write_bbl(input_files, output_name, include_titles, keep_in_preparation):
    """Write properly formatted bbl for bibentry.

    Arguments:
        input_files (list): List of bib files to load.
        output_name (str): Output file name.
        include_titles (bool): Include titles in formatting?
        keep_in_preparation (bool): Keep articles in preparation?

    Return:
        int: Number of entries loaded.

    Raise:
        KeyError: If cannot format entry.

    """
    if not output_name.endswith('.bbl'):
        output_name += 'bbl'
    entry_counter = 0
    with open(output_name, 'w') as output_file:
        output_file.write(r'\begingroup\begin{thebibliography}{1}')
        output_file.write('\n')
        output_file.write('\n')
        for file_ in input_files:
            bib_db = load_bibtex(file_)
            for entry in bib_db.entries:
                if 'note' in entry and any(note_to_skip in entry['note']
                                           for note_to_skip in ('in preparation', 'upcoming')):
                    if keep_in_preparation:
                        output_file.write(r'\bibitem{%s}' % entry['ID'])
                        output_file.write('\n')
                        output_file.write('{} (in preparation)\n'
                                          .format('\href{{{}}}{{{}}}'
                                                  .format('https://cds.cern.ch/search?ln=en&p1={}&f1=reportnumber'.format(entry['ID']),
                                                          entry['ID'])))
                        output_file.write('\n')
                    continue
                if 'TBD' in entry['title']:
                    continue
                entry_counter += 1
                output_file.write(r'\bibitem{%s}' % entry['ID'])
                output_file.write('\n')
                output_file.write('%s\n\n' % format_entry(entry, include_titles))
                output_file.write('\n')
        output_file.write('\n')
        output_file.write(r'\end{thebibliography}\endgroup')
    return entry_counter


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('output_file', action='store', type=str)
    parser.add_argument('input_files', nargs='+', action='store', type=str)
    parser.add_argument('--titles', action='store_true')
    parser.add_argument('--keep-in-preparation', action='store_true')
    args = parser.parse_args()
    # Do work!
    write_bbl(args.input_files, args.output_file, args.titles, args.keep_in_preparation)

# EOF

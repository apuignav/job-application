#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# @file   publication_print.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   07.02.2016
# =============================================================================
"""Print publication lists."""

import logging
import os

import argparse

import bibtexparser


logger = logging.getLogger('bibtexparser.bparser')
logger.addHandler(logging.StreamHandler())


def load_bibtex(bib_file):
    """Load Bibtex database.

    LHCb commands are removed.

    Arguments:
        bib_file (str): bib file to load.

    Returns:
        bibtexparser.bibdatabase.BibDatabase: Loaded database

    Raises:
        KeyError: If the journal is not recognized

    """
    def customizations(record):
        """Apply customizations to loaded records."""
        if 'title' in record:
            if r'\lhcb' in record['title']:
                record['title'] = record['title'].replace(r'\lhcb', 'LHCb')
            if not record['title'].startswith('{'):
                record['title'] = '{%s}' % record['title']
        return record

    if not os.path.exists(bib_file):
        raise OSError("Cannot find file %s" % bib_file)
    with open(bib_file) as bibtex_file:
        bib_parser = bibtexparser.bparser.BibTexParser()
        bib_parser.ignore_nonstandard_types = False
        bib_parser.customization = customizations
        bib_database = bibtexparser.load(bibtex_file, parser=bib_parser)
    return bib_database


def get_bib(input_file, only_published=False):
    """Write a filtered bib file.

    Also prints the total number of output publications.

    Arguments:
        input_file (str): bib file to load.
        only_published (bool, optional): Skip the e-print only papers.
            Defaults to False.

    Yields:
        str: Keys in order.

    """
    pub_counter = 0
    bib_db = load_bibtex(input_file)
    for entry in bib_db.entries:
        if 'note' in entry and entry['note'] == 'in preparation':
            continue
        if 'journal' not in entry and \
                (only_published or 'eprint' not in entry):
            continue
        pub_counter += 1
        yield entry['ID']
    print('Number of papers: {}'.format(pub_counter))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--only-published', action='store_true', default=False)
    parser.add_argument('-s', '--section', action='store', type=str, default='all')
    parser.add_argument('output', action='store', type=str)
    parser.add_argument('input', action='store', type=str, nargs='+')
    args = parser.parse_args()
    # Do work!
    with open(args.output, 'w') as output_file:
        output_file.write('---\n')
        output_file.write('%s:\n' % args.section)
        for input_file in args.input:
            for key in get_bib(input_file, args.only_published):
                output_file.write('    - %s\n' % key)
        output_file.write('...\n')


# EOF

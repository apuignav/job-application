#!/bin/bash

# Process arguments
output_file=$1
shift
input_files=$@

# Merge
echo "---" > $output_file
cat $input_files | grep -v -e "^---$" | grep -v -e "\.\.\." >> $output_file
echo "..." >> $output_file

# EOF


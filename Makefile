TEX = lualatex
LATEXMKOPTS = -logfilewarnings
COMMON_TEX = templates/packages.tex templates/booleans.tex templates/aesthetics.tex templates/fonts.tex templates/captions.tex templates/lhcb-symbols-def.tex templates/extra-symbols-def.tex templates/titling.tex templates/headers.tex templates/biblatex.tex
COMMON_YAML = personal.yaml
BIBFILES = bib/*.bib

# Projects
PROJECT_TEMPLATE = templates/template_project.tex
SINGLE_TEMPLATE = templates/template_singlepage.tex

.DEFAULT_GOAL := all

############################
# CV
############################
CV_YAML_COMMON = cv/awards-fellowships.yaml cv/employment_and_education.yaml cv/organization.yaml cv/research.yaml cv/conferences.yaml cv/leadership.yaml cv/outreach.yaml cv/teaching.yaml references.yaml
CV_YAML = $(COMMON_YAML) config.yaml $(CV_YAML_COMMON)

cv.bbl cv-short.bbl cv-snf.bbl: scripts/createbbl.py $(BIBFILES)
	python3 scripts/createbbl.py --keep-in-preparation $@ $(BIBFILES)

cv-with-titles.bbl: scripts/createbbl.py $(BIBFILES)
	python3 scripts/createbbl.py --keep-in-preparation --titles cv-with-titles.bbl $(BIBFILES)

.SECONDEXPANSION:
cv.pdf cv-with-titles.pdf: CV_TEMPLATE = templates/template_cv.tex 
cv-short.pdf: CV_TEMPLATE = templates/template_cv_short.tex
cv.pdf cv-with-titles.pdf: CV_MAIN_YAML = cv.yaml 
cv-short.pdf: CV_MAIN_YAML = cv_short.yaml
cv-snf.pdf: CV_TEMPLATE = templates/template_cv_snf.tex 
cv-snf.pdf: CV_MAIN_YAML = cv_snf.yaml 
cv-with-titles: CV_YAML += cv/titles-in-research.yaml

cv.pdf cv-short.pdf cv-with-titles.pdf cv-snf.pdf: %.pdf: $(COMMON_TEX) $$(CV_TEMPLATE) templates/template_cv_base.tex $$(CV_YAML) $$(CV_MAIN_YAML) %.bbl 
	scripts/merge_yaml.sh $*_temp.yaml $(CV_YAML) $(CV_MAIN_YAML)
	pandoc $*_temp.yaml -o $*.tex --template=$(CV_TEMPLATE) --fail-if-warnings
	latexmk $(LATEXMKOPTS) -$(TEX) -nobibtex $*.tex  $(CV_TEX_OPTIONS)
	latexmk $(LATEXMKOPTS) -c -$(TEX) $*.tex $(CV_TEX_OPTIONS)
	rm -f $*_temp.yaml $*.tex

cv cv-short cv-with-titles cv-snf: %: %.pdf

# Scientific achievements
SA_BASE_NAME = scientific-achievements

$(SA_BASE_NAME).bbl: cv.bbl
	cp cv.bbl $(SA_BASE_NAME).bbl

$(SA_BASE_NAME).pdf: %.pdf : $(COMMON_TEX) %.yaml templates/template_cv_base.tex templates/template_cvappendix.tex research-plan-extras.tex %.bbl
	pandoc -o $*.tex $*.yaml --template=templates/template_cvappendix.tex --pdf-engine=$(TEX) --fail-if-warnings
	latexmk $(LATEXMKOPTS) -nobibtex -$(TEX) $*.tex
	latexmk $(LATEXMKOPTS) -c -$(TEX) $*.tex
	rm -f $*.tex
$(SA_BASE_NAME): $(SA_BASE_NAME).pdf

# SNF format with achievements
cv-snf-full.pdf: cv-snf.pdf $(SA_BASE_NAME).pdf
	python3 /usr/local/bin/pdfmerge.py -o cv-snf-full.pdf cv-snf.pdf $(SA_BASE_NAME).pdf
cv-snf-full: cv-snf-full.pdf

# Non-academic CV
RESUME_YAML = resume_details.yaml cv_ind.yaml config.yaml $(COMMON_YAML)
RESUME_TEMPLATE = templates/template_cv_ind.tex
RESUME_SRC = $(COMMON_TEX) $(RESUME_TEMPLATE) templates/template_cv_base.tex $(RESUME_YAML) 
resume.pdf: %.pdf: $(RESUME_SRC) 
	pandoc $(RESUME_YAML) -o $*.pdf --pdf-engine=$(TEX) --template=$(RESUME_TEMPLATE) --fail-if-warnings
resume: resume.pdf

CV_IND_LONG_YAML = cv_ind_long_details.yaml cv_ind.yaml config.yaml $(COMMON_YAML)
CV_IND_LONG_TEMPLATE = templates/template_cv_ind.tex
CV_IND_LONG_SRC = $(COMMON_TEX) $(CV_IND_LONG_TEMPLATE) templates/template_cv_base.tex $(CV_IND_LONG_YAML) 
cv-ind-long.pdf: %.pdf: $(CV_IND_LONG_SRC) 
	pandoc $(CV_IND_LONG_YAML) -o $*.pdf --pdf-engine=$(TEX) --template=$(CV_IND_LONG_TEMPLATE) --fail-if-warnings
cv-ind-long: cv-ind-long.pdf

cv-ind: resume cv-ind-long

############################
# Publication list
############################
PUBLIST_TEMPLATE = templates/template_publist.tex
PUBLIST_YAML = publication-list.yaml $(COMMON_YAML) publication-list-full.yaml
PUBLIST_BIBS = bib/LHCb-PAPER.bib bib/LHCb-DP.bib

publication-list-full.yaml: $(PUBLIST_BIBS)
	#python3 scripts/publication_print.py --section="all" publication-list-full.yaml $(PUBLIST_BIBS)
	python3 scripts/publication_print.py --only-published --section="all" publication-list-full.yaml $(RESEARCH_OUTPUT_BIBS)

publist: $(PUBLIST_TEMPLATE) $(PUBLIST_YAML) scripts/publication_print.py
	#rm -rf research-output.{bbl,blg,log,aux}
	pandoc -o publication-list.tex $(PUBLIST_YAML) --template=$(PUBLIST_TEMPLATE) --fail-if-warnings
	latexmk $(LATEXMKOPTS) -$(TEX) publication-list.tex

############################
# Conference list
############################
CONFLIST_TEMPLATE = templates/template_conf.tex
CONFLIST_YAML = cv_details.yaml conf-list.yaml
CONFLIST_BIBS = bib/LHCb-PROC.bib

conflist: $(CONFLIST_TEMPLATE) $(CONFLIST_YAML)
	rm -rf conf-list.{bbl,blg,log,aux,bib}
	cp bib/LHCb-PROC.bib conf-list.bib
	pandoc -o conf-list.tex $(CONFLIST_YAML) --template=$(CONFLIST_TEMPLATE) --fail-if-warnings
	latexmk $(LATEXMKOPTS) -$(TEX) conf-list.tex

############################
# Research output
############################
RESEARCH_OUTPUT_TEMPLATE = templates/template_research_output.tex
RESEARCH_OUTPUT_YAML = research-output.yaml $(COMMON_YAML) publication-list-full.yaml cv/conferences.yaml cv/outreach.yaml
RESEARCH_OUTPUT_BIBS = bib/LHCb-PAPER.bib # bib/LHCb-DP.bib

research-output.pdf: $(RESEARCH_OUTPUT_TEMPLATE) $(RESEARCH_OUTPUT_YAML)
	pandoc -o research-output.tex $(RESEARCH_OUTPUT_YAML) --template=$(RESEARCH_OUTPUT_TEMPLATE) --fail-if-warnings
	latexmk $(LATEXMKOPTS) -$(TEX) research-output.tex
research-output: research-output.pdf

############################
# Cover letter
############################
COVERLETTER_TEMPLATE = templates/template_coverletter.tex
COVERLETTER_YAML = cover-letter.yaml $(COMMON_YAML)

cover-letter.pdf cover-letter-ind.pdf: %.pdf : %.md $(COVERLETTER_YAML) $(COVERLETTER_TEMPLATE) templates/letter.lco
	pandoc -o $*.pdf $*.md $(COVERLETTER_YAML) --template=$(COVERLETTER_TEMPLATE) --pdf-engine=$(TEX) --fail-if-warnings
cover-letter cover-letter-ind: % : %.pdf

############################
# References document
############################
REFERENCES_TEMPLATE = templates/template_reference.tex
REFERENCES_YAML = references.yaml $(COMMON_YAML)

references: $(REFERENCES_YAML) $(REFERENCES_TEMPLATE)
	pandoc -o references.pdf $(REFERENCES_YAML) --template=$(REFERENCES_TEMPLATE) --pdf-engine=$(TEX) --fail-if-warnings

## Examples
## Shorter document
#SYNOPSIS_INPUT = examples/synopsis.md examples/synopsis.yaml examples/project.yaml
#synopsis.tex: $(SYNOPSIS_INPUT) $(PROJECT_TEMPLATE) examples/research-plan-extras.tex
#    pandoc -o examples/synopsis.tex $(SYNOPSIS_INPUT) --template=$(PROJECT_TEMPLATE) --pdf-engine=$(TEX) --fail-if-warnings
#synopsis.pdf: synopsis.tex bibliography.bib $(BIBFILES)
#    latexmk -$(TEX) synopsis.tex
#    @texcount synopsis.tex | grep "Words in text" | sed 's/Words in text/Words in synopsis/'
#    @texcount -brief -q -char synopsis.tex 2> /dev/null | grep File | awk '{print "Chars in text (no spaces): "$1}'
#    @mdls -name kMDItemNumberOfPages synopsis.pdf | sed 's/kMDItemNumberOfPages = /Number of pages in synopsis: /'
#synopsis: synopsis.pdf

## Long project
#RESEARCH_INPUT = examples/research-plan.md examples/research-plan.yaml examples/project.yaml
#research-plan.tex: $(RESEARCH_INPUT) $(PROJECT_TEMPLATE) examples/research-plan-extras.tex
#    pandoc -o research-plan.tex $(RESEARCH_INPUT) --template=$(PROJECT_TEMPLATE) --fail-if-warnings
#research-plan.pdf: research-plan.tex $(BIBFILES) bibliography.bib
#    latexmk -lualatex research-plan.tex
#    @echo ""
#    @texcount research-plan.tex | grep "Words in text" | sed 's/Words in text/Words in research plan/'
#    @texcount -brief -q -char research-plan.tex 2> /dev/null | grep File | awk '{print "Chars in text (no spaces): "$1}'
#    @mdls -name kMDItemNumberOfPages research-plan.pdf | sed 's/kMDItemNumberOfPages = /Number of pages in research plan: /'
#research-plan: research-plan.pdf

## Career plan
#career-plan.pdf: examples/career-plan.md examples/career-plan.yaml $(SINGLE_TEMPLATE)
#    pandoc -o examples/career-plan.pdf examples/career-plan.yaml examples/career-plan.md --template=$(SINGLE_TEMPLATE)  --pdf-engine=$(TEX) --fail-if-warnings
#career-plan: career-plan.pdf


.PHONY: clean
clean :
	rm -f *.pdf *.log *.aux *.bbl *.bcf *.fdb_latexmk *.out *.run.xml *.xdv *.blg *.fls
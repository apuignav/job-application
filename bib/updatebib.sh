#!/usr/bin/env zsh

git archive --remote=ssh://git@gitlab.cern.ch:7999/lhcb-docs/templates.git --prefix=LHCb-latex-template/latest/latex/ HEAD:LHCb-latex-template/latest/latex/ |  tar xvf - && cp -r LHCb-latex-template/latest/latex/*.bib . && rm -rf LHCb-latex-template main.bib standard.bib

for filename in *.bib; do
    sed -i '' 's/\\B\\/\\PB\\/g' $filename
    sed -i '' 's/\\B\$/\\PB\$/g' $filename
    sed -i '' 's/\\tev/TeV/g' $filename
    sed -i '' 's/lhcbreport/report/g' $filename
    sed -i '' 's/Collaborastion/collaboration/g' $filename
    sed -i '' '/Month/d' $filename
    sed -i '' '/Address/d' $filename
    sed -i '' '/Institution/d' $filename
done
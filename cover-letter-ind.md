---
email: albert.puig.navarro@gmail.com
phone: false
# To
to-head: Nestlé Research Center
to:  | # Always leave 2 spaces at the end of each line!
  Route du Jorat 57  
  Vers-chez-les-Blanc  
  CH-1000 Lausanne 26  
email: albert.puig.navarro@gmail.com
subject: Application to the position of Associate Data Scientist (955346)
...

I want to introduce myself as an applicant for the position of Associate Data Scientist at the Nestlé Institute for Food Safety and Analytical Sciences.
I am especially excited about this position given its goal of helping \textsc{nifsas} teams to make data-science-driven decisions, as I believe data science will lead to more efficient and precise research.
With a \phd in Physics and more than ten years of experience in analysing extremely big and noisy datasets at \cern, I am confident I will be a valuable asset for your team.

During my career, my main focus has been the study of extremely rare events (1 every million collisions), and how to extract those from the noisy collision data collected by the \lhcb experiment at \cern.
Through this task I have acquired an extensive knowledge of machine learning techniques, and on the processing of large datasets of more than 1 Pb per year.
In addition, carrying out my scientific projects has required intense communication with detector experts in order to understand the effects of the experimental apparatus on the data, and with other scientists, with whom I've collaborated to achieve common goals.

As a postdoctoral researcher, my scientific and team-management skills have been recognised with appointments to various responsibility positions within the \lhcb collaboration.
In particular, I have been in charge of coordinating a diverse team of around 100 scientists performing different data analyses with the goal of pushing said analyses to publication stage, while at the same time acting as interface with the collaboration management team.
I have also been tasked to present the results included in these publications on behalf of the collaboration in more than twenty international conferences.

My abilities as a Data Scientist are rooted in my specialisation in experimental particle physics and in a strong computing profile.
I attribute part of my success as a data analyst to this latter strength, which has allowed me to implement complex new analysis strategies in a fast an efficient way.
I believe my personality has also played a major role in my ability to succeed in this career.
I am an extremely analytical, but at the same time I have a great facility to coordinate with and manage very diverse teams to ensure the best possible outcome in each situation.

I would like to thank you for taking the time to review my application.
I look forward to hearing more about \textsc{nifsas} and the details of the Data Scientist position.
I feel that my education and experience will ensure my success in this role.


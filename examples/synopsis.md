It is an exciting time in particle physics.
A coherent pattern of deviations from the Standard Model (\sm) has emerged in the last few years and we may be on the verge of a revolutionary paradigm shift, comparable to the discovery of the \jpsi or the observation of \CP violation.
It is therefore imperative to pursue these anomalies in the coming years to either confirm or disprove them, getting ready at the same time for the research that will come afterwards.

The goal of this project is to build a team that will play an important role in these endeavours by analysing the \lhcb data from its first two runs to confirm these anomalies, exploiting the data collected during the \lhcb upgrade \textsc{i} to characterise the New Physics (\np) phenomena arising from them, and contributing to the efforts to prepare the next generation of particle physics experiments.
This work will be carried out at the University of Zürich, which provides an ideal environment for its success thanks to its world-leading position in the flavour physics community, both in theory and experiment.

## Rare semileptonic beauty decays --- the road to New Physics

Fermions in the \sm are organized in *flavours*.
Transitions between flavours are mediated by the charged-current weak interaction bosons and their probability is controlled by the elements of the corresponding fermion mixing matrices.
As a consequence, transitions between same-charge quarks can only occur through loop processes that are predicted to be rare in the \sm due to the smallness of the off-diagonal elements of the Cabibbo-Kobayashi-Maskawa quark-mixing matrix\ \cite{Cabbibo:ckm:1963,Kobayashi:ckm:1973}.
These processes constitute an ideal laboratory to indirectly access new interactions at very high energy scales, and they may remain as the only way of studying \np if these scales are larger than those accessible by direct measurements at colliders.
In particular, rare semileptonic decays of beauty hadrons are considered one of the key sectors to look for physics beyond the \sm thanks to the fact that they can be predicted with small uncertainties and the signal yields of many modes are large enough to perform precision measurements at \lhcb and \belletwo.

Studies of these \btosll transitions using data from the first run of the \lhc have resulted in a two sets of anomalies:
on one hand, those arising from the study of angular distributions 
<!--the properties -->
of \btosmumu decays, and, on the other, those appearing in tests of the universality of the weak interaction with respect to lepton flavour (\lfu).
<!--between electrons and muons. -->
A pattern consistent with \np coupling hierarchically with lepton mass seems to be emerging from these anomalies, and most global fits, which include information from many \decay{b}{s} results from multiple experiments, can explain both sets of anomalies in a unified way\ \cite{Capdevila:2017bsm,DAmico:2017mtc,Geng:2017svp,Ciuchini:2017mik,Altmannshofer:2017yso}, hinting at a common \np origin.
<!--There exists, however, concern in the theory community about to which extent the long-distance contributions from \ccbar resonances pollute the observables and how factorisation holds in this case\ \cite{Lyon:2014hpa};-->
<!--however, these hadronic uncertainties could only affect the \sm predictions of properties of \btosdmumu and \btosdee decays, leaving \lfu tests unaffected.-->

\newpar In the first group of analyses, 
<!--the differential branching fractions of \decay{\B}{K\mumu}, \decay{\B}{\Kstar\mumu}, \decay{\Bs}{\phi\mumu}, \decay{\Lb}{\Lz\mumu}, and \decay{\Bz}{\Kpi\mumu} at large \Kpi mass\ \cite{LHCb-PAPER-2014-006,LHCb-PAPER-2014-006,LHCb-PAPER-2016-012,Chatrchyan:2013cda,Khachatryan:2015isa,LHCb-PAPER-2015-023,LHCb-PAPER-2015-009,LHCb-PAPER-2016-025} have been measured with more precision than the corresponding theoretical predictions;-->
<!--while some of these results point towards lower values than the \sm prediction, the measurements are still compatible with it.-->
<!--Additionally, -->
the rich phenomenology provided by angular distributions of \btosdll decays---especially in the case of \decay{\Bz}{\Kstarz\mumu}---has been exploited to access a wide array of different \np effects.
<!--In particular, -->
<!--while the standard angular observables measured by the \cdf, \babar, \belle, \cms and \lhcb collaborations\ \cite{Aaltonen:2011ja,Lees:2015ymt,Wei:2009zv,Khachatryan:2015isa,LHCb-PAPER-2015-051} have been found to be compatible with the \sm, -->
Of all the available angular observables, the optimized \Pfivep parameter---more sensitive to \np thanks to its smaller theoretical uncertainty due to form factor cancellations---measured by \lhcb, \belle, \cms, and \atlas\ \cite{LHCb-PAPER-2015-051,Wehle:2016yoi,CMS-PAS-BPH-15-008,ATLAS-CONF-2017-023}, presents a large local discrepancy between data and the \sm prediction, found at the level of $3.7\sigma$ in the case of \lhcb.
This discrepancy is currently considered one of the most intriguing anomalies in the field---even with some caveats regarding hadronic uncertainties\ \cite{Lyon:2014hpa}, which don't affect \lfu tests---
<!--, and its combination with branching fraction and other flavour measurements yields a global tension with the \sm of the order of $4\sigma$\ \cite{}.-->
<!--Further measurements of angular distributions carried out by \lhcb in other less sensitive \btosll modes, \decay{\B}{K\mumu}\ \cite{LHCb-PAPER-2014-007}, \decay{\Bs}{\phi\mumu}\ \cite{LHCb-PAPER-2015-023} and \decay{\Lb}{\Lz\mumu}\ \cite{LHCb-PAPER-2015-009}, don't show large deviations from the \sm.-->
<!--a smaller sensitivity is expected in these cases owing to the simpler structure of the decay, the lack of flavour tagging and the simplified angular distribution used in the analysis, respectively.-->
and an updated \Pfivep measurement adding the data collected during Run 2 will be crucial to clarify its robustness.
In addition to \decay{\Bz}{\Kstarz\mumu}, other \btosll modes have been studied in search for further anomalies\ \cite{LHCb-PAPER-2014-007,LHCb-PAPER-2015-023,LHCb-PAPER-2015-009}, but sensitivity to \np effects in those cases is reduced
<!--, \eg, due to the simpler structure of the decay or the need to know the initial flavour of the \PB meson,-->
and no other significant discrepancy has been found.

In order to address this lack of sensitive measurements, this project will study the \decay{\Bs}{\Kstarzb\mumu} decay using data collected in the first run of the \lhcb upgrade \textsc{i} (\lhcbu).
This mode is the \btodmumu counterpart to \decay{\Bz}{\Kstarz\mumu} and therefore its angular distribution has the same structure.
Its interest in the search for \np is enhanced by two additional facts:
on one hand, the comparison between \btosll and \btodll transitions provides deep insights on the flavour structure of \np explaining the anomalies\ \cite{Hurth:2013ssa};
<!--most \np models to explain the anomalies, such as those discussed in Ref.\ \cite{Buttazzo:2017ixm}, introduce the same type of beyond-the-\sm effects in \btosll and \btodll transitions, making the study of \btodll processes of the utmost importance to understand the origin of the anomalies;-->
on the other, \np effects could appear in \btodll transitions even if the anomalies were not confirmed, opening a new road to search for non-\sm effects.
Even if \decay{\Bs}{\Kstarzb\mumu} is suppressed by a factor hundred with respect to its \btosmumu equivalent, the luminosity collected in the \lhcbu will allow to collect a sample of roughly 400 signal events, equivalent to that used in the first measurement of \Pfivep\ \cite{LHCb-PAPER-2013-037}.
Using a *folding* technique like the one introduced in that publication,^[This technique allows to study, without loss in sensitivity, the most interesting angular observables by exploiting the symmetries of the differential decay rate with respect to combinations of angles to apply transformations that cancel less interesting observables.] we aim at measuring \Pfivep in \decay{\Bs}{\Kstarzb\mumu} and study its compatibility both with the \sm and \decay{\Bz}{\Kstarz\mumu}.
To address the challenge of dealing with the overlap between the \Bz and \Bs signals in relation with the smallness of the \Bs one,
<!--The main challenge of this measurement will be the size of the \Bs signal with respect to the \Bz one, and the overlap between them-->
<!--, even with the excellent resolution of the \lhcb detector;-->
<!--to alleviate this problem, -->
the angular analysis will be performed simultaneously for both modes, exploiting their similar kinematics to reduce systematic effects as much as possible.

\newpar The second set of anomalies comes from the study of \lfu:
since particles in the \sm
<!-----with the exception of the Higgs boson----->
couple with the same strength to all lepton flavours, \btosdll processes should have a decay rate independent of the lepton type, up to very small corrections due to Higgs penguin contributions and phase space differences. 
As a consequence, the ratio of decay rates
\begin{equation}
    \R{X}\equiv \frac{\Gamma(\decay{\bquark}{X\mumu})}{\Gamma(\decay{\bquark}{X\epem})},\label{eq:rx}
\end{equation}
where $X=K, \Kstar, \Pphi, \ldots$, is predicted to be $1.00\pm0.01$\ \cite{Bordone:2016gaq} and can be used as a null test of the \sm.
The \babar, \belle and \lhcb collaborations have measured \R{K} in several ranges of the dilepton mass squared (\qsq)\ \cite{PhysRevLett.103.171801,PhysRevD.86.032012,LHCb-PAPER-2014-024}, and while the \B factories didn't observe significant deviations from unity, the more precise \lhcb measurement showed a suppression of the muon channel of $2.6\sigma$ with respect to the \sm prediction.
Similarly, the measurement of \R{\Kstarz} by the same three collaborations\ \cite{Lees:2013nxa,Ijima:2009,LHCb-PAPER-2017-013}, resulted in the more precise \lhcb measurement yielding discrepancies of $2.1-2.5$ standard deviations in two separate \qsq regions.
The update of these studies with data collected in the second \lhc run, as well as the measurement of other \R{} ratios, such as \R{\phi}, \R{pK} or \R{K\pi\pi}^[The \R{K\pi\pi} measurement is currently underway in the context of my ongoing \snf Ambizione project.] will have the sensitivity to observe \np if the present deviations truly come from non-\sm effects.

To have a significant participation in this effort, one of the first tasks the team will undertake is the measurement of \R{\Kpi} in the $1330<m(\Kpi)<1530\,\mevcc$ region.
This result is not only interesting because of its contribution to the big picture of anomalies coming from \lfu tests, but also for the insights it can provide into possible \np:
as distinct \np models affect decays differently according to their spin structure\ \cite{Geng:2017svp}, the complex resonance pattern of the \Kpi system in that mass region will provide complementary information to that obtained from \R{K} and \R{\Kstarz}.
Extrapolating from the signal observed by \lhcb in Run 1\ \cite{LHCb-PAPER-2016-025}, around $860$ \decay{\Bz}{\Kpi\mumu} and $300$ \decay{\Bz}{\Kpi\epem} events are expected in the $1.1< \qsq < 6.0\,\gevgevcccc$ region when using data from the first two \lhc runs.
<!--The sensitivity of the \R{\Kpi} measurement can then be estimated by noting that -->
Since these signal estimations represent a factor $2.5$ more than what was used in the recent \R{\Kstarz} analysis, a potentially very significant deviation from the \sm can be expected from this measurement.
Following the strategy from Refs.\ \cite{LHCb-PAPER-2014-024,LHCb-PAPER-2017-013}, \R{\Kpi} will be obtained from a double ratio with the corresponding resonant \jpsi modes, 
\begin{equation}
    R_{\Kpi} = \frac{\BR(\decay{\Bz}{\Kpi\mumu})}{\BR(\decay{\Bz}{\Kpi\jpsi(\to\mumu)})} \times \frac{\BR(\decay{\Bz}{\Kpi\jpsi(\to\epem)})}{\BR(\decay{\Bz}{\Kpi\epem})},
\end{equation}
since the similarity of the signal and \jpsi modes helps to substantially reduce many systematic uncertainties coming from the differences in reconstruction between muons and electrons, mainly due to presence of \brem emissions and the trigger response.
Three improvements with respect to previous \R{X} measurements will be explored, the successful implementation of which will lead to a sizeable increase of the significance of the result: 

  + the upper boundary of the studied \qsq region, previously set at $6\,\gevgevcccc$ to reduce the leakage of the abundant \jpsi mode into the invariant mass distribution of the signal mode, will be extended to $7\,\gevgevcccc$, gaining a significant fraction of signal, by introducing extra requirements on the \qsq calculated when constraining the mass of the $\Kpi\elel$ system to the \Bz one;
  + machine learning (\ml) techniques will introduced to perform multidimensional corrections to the simulation to better reproduce the real data, replacing histogram-based ones; and
  + the knowledge of the \decay{\Bp}{\Kp\pim\pip\epem} data, currently under study, will be directly introduced into the mass fit to improve the modelling of these dangerous background, currently done by means of simulated samples that result in large systematics effects.

<!--Since at least the first two can also be adopted by all other \R{} measurements, this work will have a significant impact on the observation of \np in \lhcb.-->

Following the same motivation as the study of angular distributions in \btodmumu transitions, a test of \lfu in \btodll transitions is also proposed.
Suppressed \decay{\Bp}{\pip\elel} decays are ideal for accomplishing this goal thanks to their relatively large yield:
with \decay{\Bp}{\pip\mumu} already observed by \lhcb in Run 1\ \cite{LHCb-PAPER-2015-035}, around $60$ \decay{\Bp}{\pip\epem} events in the full \qsq range are expected just using data from Runs 1 and 2, making an observation feasible early on in the timeline of the project.
The biggest challenge in achieving this observation will be the control of the backgrounds:
on one hand, partially reconstructed backgrounds---that is, decays such as \decay{\B}{X(\to\pip\piz)\epem} in which one or more particles are not reconstructed---will be suppressed using \ml methods that exploit the differences in decay kinematics;
on the other, the contamination coming from the dominant \decay{\Bp}{\Kp\epem} mode will be controlled by performing a simultaneous fit with \decay{\Bp}{\pip\epem} and fixing its contribution through data-driven methods.

This work will serve as the baseline for the study of \R{\pi} in the $1.1 < \qsq < 6(7),\gevgevcccc$ region, impossible until the end of Run 3, in 2023.
This measurement, statistically limited by the $265$ and $60$ candidates expected at that moment, represents an important stepping stone in the study of the nature of the possible \np causing the flavour anomalies at the \lhcbu.
It will follow a similar strategy as \R{\Kpi}, benefiting from the improvements explored in that study, with possible additional gains coming from a better electron selection efficiency in the \lhcbu, which will be discussed afterwards.
<!--The possibility of measuring $\R{K} - \R{\pi}$ (that is, the difference in \lfu between \btosll and \btodll decays) using the simultaneous fit with \decay{\Bp}{\Kp\elel} will also be explored, but the sensitivity of such measurement will be extremely limited due to the number of \decay{\Bp}{\pip\epem} candidates.-->

\newpar So far, the study of \lfu at \lhcb has been limited to ratios of branching fractions, but it can be extended to angular distributions by building theoretically clean observables sensitive to the difference between muons and electrons\ \cite{Capdevila:2016ivx,Serra:2016ivr}:
only \belle has performed such study and measured the difference in the \Pfivep observable between muons and electrons in \decay{\Bz}{\Kstarz\elel}\ \cite{Wehle:2016yoi}, but the statistical precision is too low to extract any conclusions.
These angular studies are essential in the confirmation of the anomalies seen in the \R{X} ratios, since they are more robust with respect to the knowledge of the absolute efficiency in electron reconstruction.

Following the measurement of \R{\Kpi}, we plan to test \lfu in the angular distributions of \decay{\Bz}{\Kpi\elel} decays at high \Kpi mass.
This angular distribution, already measured in the case of muons in Ref.\ \cite{LHCb-PAPER-2016-025}, is described by the 41 moments in which the \CP-averaged differential decay rate can be expanded in.
While no theoretical prediction is available to evaluate the effects of \np on these moments, the measurement of a significant difference between electrons and muons in any of the moments will be a clear sign of \np.
If such a difference were to arise, the interpretation of such moments in terms of the amplitudes---for example, the fifth and tenth moments are related to the $D$-wave fraction---would also provide unique information for the characterisation of \np.
Most of the work performed for the \R{\Kpi} measurement will be directly ported to the this study, with the added challenge of the need to describe the efficiency as a function of $m(\Kpi)$ and the decay angles, determined using a large simulated sample.
Its agreement with data will be checked with the \jpsi modes, which will also be used to control the signal shapes and validate the angular fit.

## Preparing for the future --- the \lhcb upgrade and beyond

After Run 2 is finished, a two-year long shutdown is foreseen, during which the \lhcbu will be installed\ \cite{LHCb-TDR-012,LHCb-TDR-013,LHCb-TDR-014,LHCb-TDR-015,LHCb-TDR-016}.
Run 3 will then start in 2021 and data will be collected with a luminosity five times higher than the current one until the end of 2023.
<!--during this period, in which the \lhc will run with a luminosity five times higher than the current one, it is foreseen that \lhcbu will collect $\sim7\,$\invfb per year.-->
The main changes to the detector stem from the need to cope with this higher luminosity and the resulting increase in pile-up:
the hardware trigger will be replaced by a fully software-based trigger, and subdetectors will be upgraded---or replaced---to increase their granularity, their radiation hardness, and to ensure they can be read at $40\,$\mhz.
<!--Additionally, the calorimeter material budget will be reduced by removing two of its subdetectors, the \spd and the \presh, mainly used in the trigger, but also in the electromagnetic particle identification (\pid) algorithms.-->

The removal of the hardware trigger will remove the main source of inefficiency in the selection of electrons, which are the limiting factor in the study of \lfu at \lhcb.
<!--, the main source of inefficiency in the selection of electrons, entails the disappearance of the largest limitation in the study of \lfu at \lhcb, and considerably simplifies \btosdee analyses. -->
This potential improvement comes at the cost of requiring a complete rethinking of how \btosdee decays are triggered, as demands on the reconstruction become more stringent.
To make \btosdee analysis possible in the \lhcbu, the team will take a leading role in this endeavour, focusing on the \lhcb calorimeter reconstruction---necessary for the recovery of \brem photons---and the updated triggering strategy for these decays.

Currently, the calorimeter reconstruction represents a large fraction of the time spent in the software trigger, so in the \lhcbu, with a much higher input rate and a significant increase in multiplicity, it will be not be possible to continue using the present reconstruction algorithms.
Speeding up the calorimeter reconstruction is therefore crucial for the prospects of performing \lfu tests in the \lhcbu.
We aim at tackling this optimisation in two phases:
first, slow algorithms will be rewritten to make use of \textsc{simd} vectorisation, which was successfully used at \lhcb to speed up the tracking reconstruction for Run 2\ \cite{LHCb-PROC-2015-017};
afterwards, the remaining slow algorithms will be replaced with faster \ml alternatives.

Once reconstruction of electrons in the trigger is possible, work will be needed to be able to select rare semileptonic \btosdee decays in the context of the new trigger paradigm used in the \lhcbu.
In this paradigm, based on the *Turbo* stream\ \cite{LHCb-DP-2016-001}, only the particles pertaining to decay chains selected by the trigger, along with some other relevant pieces of information---from numerical variables to extra particles---are persisted to disk, resulting in a smaller event size that fits into the readout bandwidth.
Keeping the balance between event size and the quality of the physics output, the team will study and implement the best persistence strategy to allow to trigger inclusively in \epem signatures, similarly to what is currently done in the \mumu case.
Using \btosee benchmark channels, different strategies will be explored by building multivariate-based trigger selections and comparing their performance;
the most efficient one will be implemented and added to the \lhcbu trigger.

After another long shutdown, during which the \lhc, as well as the \atlas and \cms detectors will be upgraded, \lhcbu will continue to collect data until the end of Run 4, in 2029.
<!--, when a dataset of $50\,\invfb$ will have been collected.-->
After this run, and during the next long shutdown, \lhcb has expressed interest in performing a second upgrade\ \cite{CERN-LHCC-2017-003}. 
Even though this project is set in the distant future and has not yet been approved, \rd work has begun on technologies able to cope with the extreme conditions of an upgraded \lhc---or any other collider that may replace it---with special focus on calorimetry, motivated by measurements such as the ones presented in this proposal.

In a high-pileup environment, and besides the high radiation dose, the main problems affecting an electromagnetic calorimeter are the degraded resolution and loss of efficiency in cluster-finding due to the reduced shower separation, and the increase in combinatorial background due to a larger amount of reconstructed objects.
These can be tackled by developing highly radiation tolerant materials which generate smaller showers---and thus less overlap---adding timing information to separate particles coming from different collision points to reduce combinatorial background, and adding longitudinal segmentation information to measure the directionality of the shower.
Several calorimeter technologies and materials are currently being developed to this effect, but realistic studies of their impact on the physics output are paramount to guide any future decisions.
These studies, one of the main goals of this project, will need to investigate not only the effects of the technology choices but also, even more importantly, the feasibility of finding clusters in that environment.
The search for \tauthreemu, limited by the background coming from \decay{\Dsm}{\eta(\to\mumu\gamma)\mun\neumb}, which is very hard to efficiently remove without a highly performant calorimeter which allows to clearly disentangle the $\eta$, constitutes an ideal test case for such studies.
This lepton flavour violating decay, extremely suppressed in the \sm, will be one of the flagship measurements of future upgrades of \lhcb\ \cite{CERN-LHCC-2017-003} and it is also the object of a special detector layout proposal by the \ship collaboration\ \cite{Alekhin:2015byh}, called \tauship\ \cite{tauship}, as its observation would be an unambiguous sign of \np.

State-of-the-art \ml techniques, such as deep learning and convolutional neural networks, will be used to develop clustering algorithms capable of separating \decay{\Dsm}{\eta(\to\mumu\gamma)\mun\neumb} events from \tauthreemu signal.
The effect of several calorimeter features---such as granularity, timing resolution or directionality---on these algorithms will be studied on two different baseline conditions, corresponding to a high-dose region with very precise instrumentation, and a low-dose, lower precision region, typically corresponding to an outer region built with a lower budget.
We expect that the resulting algorithms will open a new page in calorimeter reconstruction, and that these studies not only will provide a critical input in the design of the \lhcb upgrade \textsc{ii} and \tauship, but also will be applicable to many future experiments.


## Making it all work --- software in \hep

One common thread to all the studies proposed in this project---and most of the life of a \hep experimentalist in general---is the reliance on software to achieve their results.
Nowadays, most \hep analyses are based on the \root framework\ \cite{root}, a monolithic software that covers such disparate topics as model fitting and generation, statistics tests, \ml algorithms and data persistence.
While this approach may have its advantages in terms of self-reliance and consistency, the reality of the scientific computing ecosystem, especially in the \python community, has taken a completely different direction, with many open source, well documented, specialised tools^[Examples include packages like `numpy` and `pandas` inside the `SciPy` ecosystem\ \cite{scipy} for data processing, `scikit-learn`\ \cite{scikit-learn} for machine-learning (\ml) applications, and `tensorflow`\ \cite{tensorflow2015-whitepaper} or `PyTorch`\ \cite{pytorch} for advanced deep-learning algorithms, as well as data formats like `HDF5`\ \cite{hdf5} and *Apache Parquet*\ \cite{apache-parquet}.] that have quickly have become standard both in academia and in industry. 

The integration of these tools---in most cases easily interchangeable thanks to the use of similar interfaces---in the daily workflow of the analyst is currently cumbersome, as \root doesn't provide generic integration with external packages.
Since replacing parts of the usual workflow with these (in many cases) superior tools is very difficult, more and more analysts are opting to reduce the usage of \root to the minimum and perform their studies building on these scientific computing packages. 
This strategy also has its own issues, mainly in terms of discovery and integration, and lack of essential tools for \hep analysis.
Many of these problems have been solved in the astronomy community by creating *Astropy*, a "community effort to develop a common core package for Astronomy in Python and foster an ecosystem of interoperable astronomy packages"\ \cite{astropy}.
Work has started in the \hep community to develop a similar package, with the *Scikit-HEP* project\ \cite{scikit-hep}, an inter-experiment effort designed to provide common tools for the community and improve on the discoverability of utility packages.

We aim at taking a leading role in this effort by developing a library for model fitting and generation, based on the following guidelines:
to be purely \python-based, to be simple^[Simplicity in this case means that the tool will be designed to do one and only one job: model fitting and generation.], to allow the exchange of minimisers through a common interface, and to be performant with heterogeneous computing resources, such as \cpu, \gpu, or many-core \cpu.
To satisfy these specifications, the use of frameworks such as `tensorflow`\ \cite{tensorflow2015-whitepaper}, currently under use in \lhcb for amplitude analysis applications, or `PyTorch`\ \cite{pytorch} will be explored.
The physics analyses proposed in previous sections will be used as test cases for the package, which will developed in parallel to them, benefiting from a continuous feedback loop that will ensure its performance and usability.


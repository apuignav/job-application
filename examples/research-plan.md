Summary
=======

The Standard Model (\sm) of particle physics represents our most fundamental knowledge of elementary particles and their interactions. 
Despite having been successfully validated by many experiments---for example with the recent discovery of the Higgs boson---the \sm fails to include essential elements such as gravity, dark matter and energy, or neutrino oscillations.
While searches for non-\sm physics phenomena that could help explain these issues have been---so far---unsuccessfully carried out in the \lhc, a large room for sizable new physics contributions remains.
It is therefore necessary to continue collecting data and pursuing the small but not-yet-significant anomalies that have been observed, such as the promising hint of a new resonance decaying into two photons observed by \atlas and \cms in data collected by the \lhc in 2015.

Rare decays of beauty mesons, which only happen at loop level, constitute an ideal laboratory to test the \sm since they allow to indirectly probe higher energy scales.
Studies of these processes---mainly by the \babar, \belle and \lhcb collaborations---have already resulted in anomalies which may point to new physics phenomena.
In particular, the deviations from lepton flavor universality observed in \decay{\Bp}{\Kp \ellp\ellm} and \decay{\B}{\Dorstar\tau\nu} decays indicate that new physics could couple hierarchically with the lepton flavor---strongly with taus, less with muons and negligibly with electrons.
Further measurements of \decay{b}{s\ellp\ellm} decays---especially those including tau leptons in the final state---are needed to disentangle the true reach of these anomalies and to significantly assert the existence of non-\sm physics.
Additionally, these new phenomena may also be accompanied by the existence of lepton flavor violating transitions such as \decay{\Bp}{\Kp e^+\mun}, forbidden in the \sm;
the search for this type of decays is therefore very interesting, as their observation would constitute an unequivocal sign of the existence of physics beyond the \sm.

New physics may also exist in less studied corners of the \sm, such as right-handed currents that require a $W$-type boson with a right-handed charged coupling to the quarks.
While direct searches for such boson have been unsuccessful, there is still a large parameter space left to explore.
The study of the polarization of the photon in \decay{b}{s\gamma} transitions represents an indirect and complementary approach to the problem and serves as a null test of the \sm, which predicts the photon to be almost fully left-polarized.

With these ideas in mind, this project focuses on two complementary lines of research, which will use data from both runs of the \lhc:
the measurement of the photon polarization in \decay{\Bp}{\Kp\pim\pip\gamma} decays to improve the constraints on right-handed currents, and the study of \decay{b}{s \ellm\ell^{(\prime)+}} decays to test lepton flavor universality and search for lepton flavor violation.
If the new physics pattern suggested by the current anomalies is confirmed by the study of the well established muon and electron channels, the golden modes for the characterization of these phenomena will be those including taus.
Therefore, it is also the aim of this project to expand the reach of \lhcb in these more challenging tau modes, paving the way to a whole new range of measurements towards the consolidation of non-\sm physics.

At the end of the project, it is expected that the obtained results---in combination with other measurements from \lhcb---will allow to elucidate if the current hints of new physics in \B decays are real cracks in the \sm or just a statistical fluctuation, thus helping shape the future of particle physics.

\pagebreak

Research plan
=============

Fermions in the Standard Model (\sm) are organized in *flavors*.
Transitions from one flavor to another are mediated by the charged-current weak interaction bosons with a probability controlled by the elements of the corresponding fermion mixing matrices.
As a consequence, change of flavor between same-charge quarks---known as *flavor changing neutral currents* (\fcnc)---can only occur through loop processes, which are predicted to be rare in the \sm due to the smallness of the off-diagonal elements of the Cabibbo-Kobayashi-Maskawa quark-mixing matrix.
These type of processes allow to gain insight into very high energy scales through indirect effects of new interactions, surpassing in many the constraints obtained from direct searches.
Rare decays of $b$ hadrons are among the most interesting \fcnc since many of the related observables can be predicted with small uncertainties while being very sensitive to new physics effects.

Even if most \lhc measurements have been found to be consistent with the \sm\ \cite{Halkiadakis:2014qda,Isidori:1647915}, the anomalies observed in \decay{b}{s\ellp\ellm} and \decay{\B}{\Dorstar\tau\nu} decays point to a pattern consistent with new physics that couples hierarchically with the lepton mass.
The current level of precision is not enough to confirm these results, so further studies are required---adding both new decay modes and data from the second run of the \lhc. 

This project focuses on the study of two of the most promising \fcnc topics in the search for non-\sm physics:
the measurement of the photon polarization to constrain the existence of right-handed currents, and the extensive study of \decay{b}{s\ellp\ellm} transitions, including tau decays, to clarify the observed anomalies and characterize the new physics effects.
The ambitious and extensive nature of this project---as well as its importance within flavor physics---requires additional personnel in order to achieve all the set goals;
the tasks to be performed by the requested PhD student are discussed in the following sections.

An ideal place to undertake this challenging project is the University of Zürich, in particular the group of Prof. Nicola Serra.
This group has been one of the main actors in the angular study of \decay{\Bz}{\Kstarz\mup\mun} decay at \lhcb\ \cite{LHCb-PAPER-2013-037}---the first place where the \decay{b}{s\ellp\ellm} anomalies where observed---as well as the searches for lepton flavor violation in tau decays\ \cite{LHCb-PAPER-2014-052}. 
Their extensive expertise in the study of rare \B decays will be a perfect environment for the development of the project.
Additionally, the University of Zürich hosts a very strong theoretical group with many renowned experts in flavor physics, such as Prof. Gino Isidori, the collaboration of whom will be essential in the interpretation of the obtained results.



State of research
-----------------

### Photon polarization

In the \sm, $W$ bosons couple only left-handedly to quarks.
As a consequence, photons emitted in \fcnc \decay{b}{s\gamma} transitions (Fig.\ \ref{fig:gamma:feynman}), are predominantly left-handed.
Several extensions of the \sm\ \cite{Pati:1974yy,*Mohapatra:1974gc,*Mohapatra:1974hk,*Senjanovic:1975rk,*Senjanovic:1978ev,*Mohapatra:1980yp,*Lim:1981kv,*Everett:2001yy}  predict that the photon can acquire a significant right-handed component through a heavy particle that couples right-handedly to quarks. 
For example, in the Left-Right Symmetric Model a $W$-type boson is predicted to give rise to large right-handed currents;
even if direct searches for this boson at \atlas and \cms has been unsuccessful, the latest limits on its mass and coupling\ \cite{Aad:2012dm,Chatrchyan:2012meb} leave ample room for non-\sm effects.
Indirect constraints on right-handed currents---coming mainly from the inclusive \decay{b}{s\gamma} branching fraction, as well as the mass difference $\Delta m_s$ and $CP$ violating phase $\phi_s$ in the \Bs system---also leave a large fraction of the space for non-\sm right-handed currents unexplored.

\begin{figure}[tb]
\centering
\begin{tikzpicture}
\begin{feynman}
    \vertex (b1) {$\scriptstyle b$};
    \vertex [right=of b1] (b2);
    \vertex [right=of b2] (b3);
    \vertex [right=of b3] (b4) {$\scriptstyle s$};

    \vertex at ($(b2)!0.5!(b3)!0.7cm!-90:(b3)$) (g1);
    \vertex [below right=1.5em and 3em of g1] (o1) {$\scriptstyle \gamma$};

    \diagram* {
      (b1) -- [fermion] (b2) -- [boson, quarter right, edge label'={$\scriptstyle W^-$}] (g1) -- [boson, quarter right] (b3) -- [fermion] (b4),
      (b2) -- [fermion, edge label={$\scriptstyle u,c,t$}] (b3),
      (g1) -- [photon] (o1),
    };
\end{feynman}
\end{tikzpicture}\qquad
\begin{tikzpicture}
\begin{feynman}
    \vertex (b1) {$\scriptstyle b$};
    \vertex [right=of b1] (b2);
    \vertex [right=of b2] (b3);
    \vertex [right=of b3] (b4) {$\scriptstyle s$};

    \vertex at ($(b2)!0.5!(b3)!0.7cm!-90:(b3)$) (g1);
    \vertex [below right=1.5em and 3em of g1] (o1) {$\scriptstyle \gamma$};

    \diagram* {
      (b1) -- [fermion] (b2) -- [boson, edge label={$\scriptstyle W^-$}] (b3) -- [fermion] (b4),
      (b2) -- [fermion, quarter right, edge label'={$\scriptstyle u,c,t$}] (g1) -- [fermion, quarter right] (b3),
      (g1) -- [photon] (o1),
    };
\end{feynman}
\end{tikzpicture}

\caption{Dominant Feynman diagrams in \decay{b}{s\gamma} transitions.\label{fig:gamma:feynman}}
\end{figure}


The photon polarization---the asymmetry between the right- and left-handed components of the photon---can be used to indirectly constrain non-\sm right-handed currents.
A precise measurement has not been performed yet---leaving this observable as one of the largest unexplored features of the \sm---but several methods to achieve the result have been proposed:

  * The time dependent \CP asymmetry of \decay{\Bz}{f_{\CP}\gamma}, where $f_{\CP}$ is a \CP eigenstate, arises from the interference between decay amplitudes with and without \Bs--\Bsb mixing and is predicted to be small in the \sm\ \cite{PhysRevLett.79.185,*Ball:2006cva,*PhysRevD.73.114022};
  a large asymmetry due to interference between the \Bz mixing and decay diagrams can only be present if the two photon helicities contribute to both \Bz and \Bzb decays.
  The \babar and \belle collaborations have studied the \decay{\Bz}{\KS\piz\gamma}\ \cite{PhysRevD.78.071102,PhysRevD.74.111104} and \decay{\Bz}{\KS\pip\pim\gamma}\ \cite{Sanchez:2015pxu,PhysRevLett.101.251601} modes and have obtained results compatible with the \sm within large uncertainties.
  Given the challenges in studying these decays in \lhcb---especially the former---it is not expected that the precision will improve until \belle\ \textsc{ii} collects a significant amount of data.

  * The angular correlations among the three-body decay products of a kaonic resonance in \decay{\B}{K_{\mathrm{res}}(\to K\pi\pi)\gamma} allow to measure directly the fundamental parameter in the effective radiative weak Hamiltonian describing the photon polarization\ \cite{Gronau:photon-polarization-k11400:2002}.
  The extremely rich spectrum of kaonic resonances in these decays---including the interference among them---makes it challenging to extract the polarization parameter because it is necessary to separate each contribution.
  At the same time, it is worth noting that interference is necessary in order for the photon polarization to be observable\ \cite{Gronau:photon-polarization-k11400:2002,Kou:photon-polarization-k1:2010}.
  In a first step towards the photon polarization measurement, \lhcb observed nonzero photon polarization for the first time by studying the photon angular distribution in bins of $\Kp\pim\pip$ invariant mass\ \cite{LHCb-PAPER-2014-001}, but the determination of the value of this polarization could not be performed due to the lack of knowledge of the hadronic system.
  The \babar and \belle collaborations have studied the $\Kp\pim\pip$ invariant mass spectrum with simplified models\ \cite{Nishida:2008,Sanchez:2015pxu}, but a full amplitude analysis is needed in order to disentangle all contributions and measure the photon polarization.

  * The transverse asymmetries of \decay{\Bz}{\Kstarz e^+ e^-} are highly sensitive to the \decay{b}{s\gamma} process at low dielectron invariant mass squared ($q^2$)\ \cite{Melikhov:1998cd,*PhysRevD.71.094009}.
The \lhcb collaboration has studied these decays with the first \lhc run dataset and has found these asymmetries to be consistent with the \sm prediction, $A^{(2)}_{T}=-0.23\pm0.23\pm0.05$ and $A^{(\mathrm{Im})}_{T}=0.14\pm0.22\pm0.05$\ \cite{Aaij:2015dea}.

Each of these measurements constrains right-handed currents differently\ \cite{Becirevic:2012dx,*Kou:2013gna}, so their complementarity can be exploited through global fits of Wilson coefficients, \eg, see Fig.\ 7 in Ref.\ \cite{Blake:2015tda}.
Currently all results are consistent with the \sm, but large uncertainties preclude us from ruling out a sizable right-handed polarization.
While further studies of the \decay{\Bz}{\KS\piz\gamma} and \decay{\Bz}{\Kstarz e^+ e^-} are set in the future---with \belle\ \textsc{ii} and the second run of the \lhc, respectively---the \decay{\Bp}{\Kp\pim\pip\gamma} dataset collected by \lhcb has not yet been used up to its full potential, making it an excellent candidate for significantly improving the constraints on right-handed currents in a much shorter time scale.

### Lepton flavor universality and lepton flavor violation

Particles in the \sm---with the exception of the Higgs boson---couple with the same strength to all lepton flavors.
This means that \decay{b}{s\ellp\ellm} processes, shown in Fig.\ \ref{fig:lfu:feynman}, should have a decay rate independent of the lepton flavor, up to very small corrections due to Higgs penguin contributions and phase space differences. 
Therefore, the ratio of decay rates
$$
R_{H_s}\equiv \frac{\Gamma(\decay{\B}{H_s\mup\mun})}{\Gamma(\decay{\B}{H_s e^+ e^-})},
$$
where $H_s=K, \Kstar, \Pphi, \ldots$, can be used as a test of lepton flavor universality in the \sm.

\begin{figure}[tbp]
\centering
\begin{tikzpicture}
\begin{feynman}
    \vertex (b1) {$\scriptstyle b$};
    \vertex [right=of b1] (b2);
    \vertex [right=of b2] (b3);
    \vertex [right=of b3] (b4) {$\scriptstyle s$};


    \vertex at ($(b2)!0.5!(b3)!0.7cm!-90:(b3)$) (g1);
    \vertex [below=4em of b3] (g2);
    \vertex [below=3em of b4] (l1) {$\scriptstyle \ellp$};
    \vertex [below=2em of l1] (l2) {$\scriptstyle \ellm$};

    \diagram* {
      (b1) -- [fermion] (b2) -- [fermion, edge label={$\scriptstyle u,c,t$}] (b3) -- [fermion] (b4),
      (b2) -- [boson, quarter right, edge label'=$\scriptstyle W^-$] (g1) -- [boson, quarter right] (b3),
      (g1) -- [photon, bend right, edge label'=$\scriptstyle \gamma/Z^0$] (g2),
      (l1) -- [fermion] (g2) -- [fermion] (l2),
    };
\end{feynman}
\end{tikzpicture}\quad
\begin{tikzpicture}
\begin{feynman}
    \vertex (b1) {$\scriptstyle b$};
    \vertex [right=of b1] (b2);
    \vertex [right=of b2] (b3);
    \vertex [right=of b3] (b4) {$\scriptstyle s$};

    \vertex at ($(b2)!0.5!(b3)!0.7cm!-90:(b3)$) (g1);
    \vertex [below=4em of b3] (g2);
    \vertex [below=3em of b4] (l1) {$\scriptstyle \ellp$};
    \vertex [below=2em of l1] (l2) {$\scriptstyle \ellm$};

    \diagram* {
      (b1) -- [fermion] (b2) -- [boson, edge label={$\scriptstyle W^-$}] (b3) -- [fermion] (b4),
      (b2) -- [fermion, quarter right, edge label'={$\scriptstyle u,c,t$}] (g1) -- [fermion, quarter right] (b3),
      (g1) -- [photon, bend right, edge label'={$\scriptstyle \gamma/Z^0$}] (g2),
      (l1) -- [fermion] (g2) -- [fermion] (l2),
    };
\end{feynman}
\end{tikzpicture}\quad
\begin{tikzpicture}
\begin{feynman}
    \vertex (b1) {$\scriptstyle b$};
    \vertex [right=of b1] (b2);
    \vertex [right=of b2] (b3);
    \vertex [right=of b3] (b4) {$\scriptstyle s$};

    \vertex [below=2em of b2] (g1);
    \vertex [below=2em of b3] (g2);
    \vertex [below=3.5em of b4] (l1) {$\scriptstyle \ellp$};
    \vertex [below=1.5em of l1] (l2) {$\scriptstyle \ellm$};

    \diagram* {
      (b1) -- [fermion] (b2) -- [fermion, edge label={$\scriptstyle u,c,t$}] (b3) -- [fermion] (b4),
      (b2) -- [boson, edge label'=$\scriptstyle W^+$] (g1) -- [fermion, edge label=$\scriptstyle \nu_\ell$] (g2) -- [boson, edge label'=$\scriptstyle W^-$] (b3),
      (l2) -- [fermion] (g1),
      (g2) -- [fermion] (l1),
    };
\end{feynman}
\end{tikzpicture}

\caption{Dominant Feynman diagrams in \decay{b}{s\ellp\ellm} transitions.\label{fig:lfu:feynman}}

\end{figure}

The \babar, \belle and \lhcb collaborations have measured $R_K$ in different ranges of $q^2$:
<!-- , and their results are summarized in Fig.\ \ref{fig:lfu:experiments}. -->
while the \B factories didn't observe significant deviations from unity\ \cite{PhysRevLett.103.171801,PhysRevD.86.032012}, the more precise \lhcb measurement of $R_K = 0.745^{+0.090}_{-0.074}\pm0.036$ shows a suppression of the muon channel of $2.6\sigma$ with respect to the \sm prediction of $R_K=1.0003\pm0.0001$\ \cite{Bobeth:2007dw}.
Additionally, \babar and \belle have measured $R_{\Kstarz}$---with large uncertainties---and have also observed a suppression of the muon modes in inclusive measurements at the level of $2\sigma$\ \cite{Lees:2013nxa,Ijima:2009}.

<!-- \begin{figure}[htbp] -->
<!-- \centering -->
<!-- \includegraphics[width=0.6\textwidth]{figs/lfu_experiments} -->
<!-- \caption{Results of the \lhcb, \babar and \belle $R_K$ measurements as a function of $q^2$ (taken from\ \cite{Aaij:1711787}).\label{fig:lfu:experiments}} -->
<!-- \end{figure} -->

While no firm conclusions can be extracted at the moment, these results, combined with the anomalies in the angular observables of \decay{\Bz}{\Kstarz\mup\mun} observed by \lhcb\ \cite{LHCB-PAPER-2015-051} and the tau enhancement in \decay{\B}{\Dorstar\tau\nu}\ \cite{PhysRevLett.109.101802,PhysRevD.88.072012,Aaij:2029609,HFAG}, could point to physics beyond the \sm in the flavor sector.
A proposed explanation for these results is the existence of a $1-50\,$TeV leptoquark with the quantum numbers of a right-handed down quark\ \cite{Bauer:2015knc}.
While direct searches have already put a lower limit to its mass at $\sim1\,$TeV\ \cite{Aad:2015caa,Khachatryan:2015vaa}, large masses may not be accessible directly at the \lhc, leaving indirect searches through \fcnc as the only possibility for their detection.

The addition of further \decay{b}{s\ellp\ellm} channels is crucial to confirm and interpret these promising results, since other $R_{H_s}$ ratios are sensitive to different combinations of couplings with respect to $R_K$ and provide a complementary constraint to new physics effects\ \cite{Hiller:2014ula}.
Even more interestingly, deviations from unity in any of the double ratios $R_{H_s}/R_K$ necessarily imply new physics in right handed currents, making them the ideal tool to clarify the nature of the observed anomalies.

In this context, the \lhcb collaboration can profit from the large dataset collected during the first run of the \lhc to measure several of these ratios.
With a large effort currently ongoing, it is expected to reduce the uncertainty on $R_{\Kstarz}$ by at least a factor of two with respect to the \B factories.
Other ratios, such as $R_\phi$ or $R_{K\pi\pi}$, also need to be measured;
while the former suffers from a small branching fractions, the large yields of  \decay{\Bp}{\Kp\pim\pip\ellp\ellm} make it a very attractive subject for a full study, also using data from the second \lhc run.

As previously discussed, the pattern of anomalies in \decay{b}{s\ellp\ellm} and \decay{\B}{\Dorstar\tau\nu} suggests that violation of lepton flavor universality is controlled by the lepton masses.
As a consequence, tauonic \B decays, such as \decay{\Bs}{\tau\tau}, \decay{\Bz}{\Kstarz\tau\tau} and \decay{\B}{K\nu\nu}, receive a strong enhancement, which could be of up to three orders of magnitude\ \cite{Alonso:2015sja}.
It is therefore crucial to also study these processes in order to better understand the observed anomalies and to identify the category of new physics models to be considered. 

Rare \decay{b}{s\tau\tau} processes are poorly constrained experimentally, $\BR(\decay{\Bp}{\Kp\tau\tau}) < 3.3\times10^{-3}$\ \cite{Flood:2010zz}, mostly due to the challenging $\tau$ reconstruction.
However, considering that \decay{b}{s\tau\tau} transitions are predicted to have \sm branching fractions of $\mathcal{O}(10^{-7})$, a substantial enhancement, along with $\tau$ reconstruction improvements, could allow their observation in \lhcb.

Finally, if lepton flavor nonuniversality is confirmed, lepton flavor violation (\lfv) is likely to arise from the same new physics sector, providing a clear proof of its existence.
In particular, in the case of a high-mass leptoquark, out of the reach of direct searches, \lfv channels such as \decay{\B}{K\mup e^-} "could be around the corner"\ \cite{Varzielas:2015iva}.
Limits on \decay{b}{s\mup e^-} decays are currently set at $\BR(\decay{\B}{K\mup e^-})<3.8\times10^{-8}$ and $\BR(\decay{\B}{\Kstar\mup e^-})<5.1\times10^{-7}$ at $90\,\%\,$CL\ \cite{Aubert:2006vb}, so they could be accessible at \lhcb by combining the data from both \lhc runs.


State of personal research
--------------------------

I have been a member of the LHCb collaboration since 2007.
As such, I have been involved in a wide range of research activities---including hardware, software and physics analysis---with special focus on radiative decays of \B mesons.

I had an important role in the first measurements of radiative decays in \lhcb, which required the development of the full analysis chain, from the selection performed in real time during data taking---the *trigger*---to the final mass fits, along with the corresponding calibration of the calorimeter to properly reconstruct the final state photons.
This resulted in the first \lhcb analyses of the \decay{\Bz}{\Kstarz\gamma} and \decay{\Bs}{\phi\gamma} decays, including the best world measurements of the \CP asymmetry of the former and of the branching fraction of the latter\ \cite{LHCb-PAPER-2011-042,LHCb-PAPER-2012-019}, the update of which I'm currently working on.

From 2013 to 2015 I coordinated the radiative decays working group---with around 15 members---taking responsibility for its analyses and the preparation for the second \lhc run.
During this time, I led the first studies towards the measurement of photon polarization with the \decay{\Bp}{\Kp\pim\pip\gamma} decay, which culminated in the observation of a non-zero polarization\ \cite{LHCb-PAPER-2014-001}.
No polarization value could be extracted---with the techniques used in the publication---due to the complex $\Kp\pim\pip$ structure, so I focused my work in transitioning to a full amplitude analysis in which this will be possible.
This is an extremely challenging analysis---no such type of analysis has ever been performed in radiative decays---and therefore several intermediate steps needed to be taken.
As a first step, an amplitude analysis of the $\Kp\pim\pip$ system was performed, where each of the components in the $\Kp\pim\pip$ spectrum, along with their interferences, could be disentangled\ \cite{giovanni-thesis}.
Currently, the amplitude analysis is being extended to establish the sensitivity to the photon polarization observable.

In addition to this ongoing analysis, I have participated---as MSc supervisor---in two searches for rare and unobserved radiative channels, \decay{\Bs}{\phi\phi\gamma} and \decay{\Lb}{\Lambda^0\gamma}\ \cite{violaine-master,aurelie-thesis}, and I kickstarted---also as MSc supervisor---the study of the photon polarization with the time dependent \CP asymmetry in \decay{\Bs}{\phi\gamma}\ \cite{eleonie-thesis}, the last of the \lhcb golden modes of the first \lhc run\ \cite{Adeva:2009ny}.

Besides working on physics analysis, I have taken important roles in the preparation and processing of the data to be analyzed, mainly in the context of radiative decays.
One of the specificities of rare decays, and radiative decays in particular, is the need for a very efficient selection requirements due to the very small signal that can be collected;
this is especially important at the trigger level, where a decision of which collisions to store is made and inefficiencies cannot be recovered.
I have been in charge of the development of the trigger strategies for radiative decays since 2010, during which period simple selections for benchmark channels were replaced by an inclusive approach\ \cite{Puig:1426295} to expand the radiative decays physics program to unforeseen channels, such as \decay{\Bp}{\Kp\pim\pip\gamma}.
I completed the expertise acquired with these developments with a more global vision by writing a review of the \lhcb trigger strategy during the first \lhc run\ \cite{Puig:1970930}.
I was also in charge of developing the radiative decays selections used to filter the recorded data into files that can be processed by the analysts---the *Stripping*---which take an inclusive approach similar to the trigger.

Finally, since the beginning of 2016 I am coordinating the \lhcb rare decays working group---with around 80 members---which englobes the research of several institutes in some of the key research topics of the \lhcb collaboration.
This puts me in an excellent position to identify the most important \fcnc studies to be performed in the collaboration and to understand which are the least explored areas where a significant contribution can be made.

Detailed research plan
----------------------

### Photon polarization with \decay{\Bp}{\Kp\pim\pip\gamma}

The first goal of this project is to measure the photon polarization with the \decay{\Bp}{\Kp\pim\pip\gamma} decay through an amplitude analysis of its five degrees of freedom:
three squared invariant masses and two angles that define the direction of the photon with respect to the decay plane of the hadronic system.
Before performing this amplitude fit, several intermediate studies need to be carried out:

  - The \minttwo amplitude analysis software framework---developed in the \cleo collaboration for the \decay{\Dz}{\Kp\Km\pip\pim} analysis\ \cite{Artuso:2012df}---was chosen to perform the analysis due to its tight integration with the \lhcb software and its powerful capabilities in handling complex amplitude fits.
  In its current form, it presents several important limitations, the most important of which being that the polarization of the photon is not explicitly implemented, so it needs to be manually fixed by combining left- and right-handed amplitudes;
  it is also very cumbersome to add new amplitudes and fit different models in parallel.
  These shortcomings are inherent to the code structure of the package and therefore effort needs to be made to restructure and possibly rewrite parts of the code base;
  this change would benefit many \lhcb analyses which make use of \minttwo for their fits.
  This effort is going to be shared between the PhD student and me: despite being highly technical work, it is very important to become deeply familiar with the tools and will serve as a good introduction to amplitude analysis for the student.

  - The event selection used in Ref.\ \cite{LHCb-PAPER-2014-001} made use of a boosted decision tree (\bdt) to separate signal from background.
  However, its optimization was not performed with an amplitude analysis in mind and therefore a non-uniform efficiency in the fit variables is expected;
  this can potentially increase systematic uncertainties due to mismodeling of such efficiency.
  The student will be in charge of performing a reoptimization of the selection procedure with the aim of improving the signal efficiency while avoiding large nonuniformities;
  to that effect, the *uBoost* boosting method\ \cite{Stevens:2013dya} will be tested and compared to the usual boosting mechanisms.
  These modifications in the selection criteria will entail the reevaluation of the contamination from different backgrounds and the invariant \B mass fit used to extract the signal component on which to perform the amplitude fit.

  - The inclusion of data from the second run of the \lhc will require a careful study of the new dataset, since
  (*a*) the new running conditions may have altered the proportion of signal and combinatorial background, and a new \bdt may be necessary;
  (*b*) the calorimeter performance may have changed, affecting the \B mass resolution; and
  (*c*) the new trigger algorithms may have changed the signal efficiency and its uniformity.
  After evaluation of these points, a decision will be made to either merge the two runs or perform a simultaneous fit of the two datasets. 

  - Fully simulated events will be compared with generator-level data in bins of the fit observables to build a five-dimensional selection efficiency map, essential to ensure a proper fit to data.
  The presence of large peaks with long tails, shown in Figs.\ 5.3 and 5.10 of Ref.\ \cite{giovanni-thesis}, requires a specialized simulation in order to attain small uncertainties in the efficiency map with a reasonable number of simulated events---a flat, non-resonant model yields too many events in the tails and too few in the peaks, the most important part of the phase space.
  A simulation model based on the three-dimensional mass fits of Ref.\ \cite{giovanni-thesis} will be implemented and used for generation of the fully simulated sample, ensuring an optimal coverage of the efficiency map.

As soon as the \minttwo software is ready, the amplitude model will be defined---in a collaborative work between the PhD student and me---using as references the \lhcb sensitivity study and the \belle analysis of the \decay{\Bp}{\Kp\pim\pip\jpsi} mode\ \cite{belle:exp-kpipidalitz:2010};
the addition of $D$-wave components will also be assessed, as recommended in Refs.\ \cite{Gronau:photon-polarization-k11400:2002,Kou:photon-polarization-k1:2010}.
The amplitude model will then be tested for stability and proper error calculation by the student using simulated pseudoexperiments.
Once confirmed to be working, it will be applied to data in order to measure the photon polarization and to determine the branching fractions of \decay{\Bp}{K_{\mathrm{res}}^+\gamma} for each of the included kaonic resonances $K_{\mathrm{res}}$.

The systematic uncertainties in the determination of the final results include, but are not limited to:
(*a*) the differences between data and simulation, which can affect the efficiency map;
(*b*) the signal and background models in the \B invariant mass fit;
(*c*) the poor knowledge of the mass and natural width of some of the kaonic resonances included in the model; and
(*d*) the compatibility of the data with alternative fit models.
The study of these will be shared between the student and me.

### Lepton flavor universality with \decay{\Bp}{\Kp\pim\pip\ellp\ellm}

Lepton flavor universality will be tested by measuring the ratio $R_{\Kp\pim\pip}$ in regions of dilepton invariant mass squared $q^2$ chosen in order to facilitate the theoretical interpretation of the result.
Each of these regions is dominated by different physics effects and will have different kinematics, so different selection requirements will be necessary in each of them.
Resonant modes---those that reach the same final state via a \decay{\jpsi}{\ellp\ellm} resonance---will be used as control samples to reduce systematic uncertainties and to aid in the selection and invariant mass fit optimization.

The first step in the study will be to prepare a *Stripping* selection to filter the collected data and gain access to it.
This action will be taken as early as possible in order to ensure a prompt access to data collected during 2016.

Simulation will be extensively used during the analysis so it is crucial that it reliably models the data.
Variables related to detector occupancy, as well as those related to the description of the kinematics of the decay, are sensitive to mismodeling, so simulation will be reweighed by the student to better match recorded data before any further studies.

In order to determine the signal yields, the student will be in charge of several actions: 

  - A selection will be developed, both for the resonant and the non-resonant modes:
  a common \bdt will be used for all $q^2$ bins, trained with simulated signal and data background.
  The requirement on its output will be optimized, along with particle identification requirements and separately for each bin, in order to maximize the signal significance.

  - Once the selection is established, a detailed study of all physics backgrounds---\B or \Lb decays that can be misreconstructed as the signal---will be performed, applying vetoes when possible or otherwise modeling their shape when reconstructed as signal.
  Special care will be taken with the \decay{\Bp}{\Kp\pim\pip\gamma} decay, which affects the electron mode at very low $q^2$ and needs to be treated very carefully.
  
  - With a good knowledge of the signal and background contributions, a fit to the 4-body invariant mass will be prepared.
  It will consist on a simultaneous fit of the resonant and non-resonant samples in each of the $q^2$ bins, possibly also separating the electron modes in categories according to how the candidate was triggered\footnote{This technique was already used in the \decay{\Bz}{\Kstarz e^+e^-} analysis\ \cite{LHCb-PAPER-2014-066}.}.
  The simultaneous fit allows to share parameters between categories and improve the quality and stability of the fit.

Determination of the selection efficiency for each channel is also essential for the measurement and I will be in charge of it.
Similarly to previous analyses, it will be determined from reweighed simulation with the exception of particle identification---known to be badly modeled in simulation and thus determined with data-driven methods.
Due to bad $q^2$ resolution in the electron modes as a consequence of bremsstrahlung radiation, migration between $q^2$ bins will also need to be included in the efficiency calculations.

The main systematic uncertainties to consider cover (*a*) the signal and background models, which can affect the fitted yields; (*b*) the efficiency determination, mainly due to limited simulation statistics and residual data/simulation differences; and (*c*) the bin migration, which strongly depends on the decay model used in the simulation. 

### Lepton flavor violation with \decay{\Bp}{\Kp\pim\pip\mup e^-}

The search for the lepton flavor violating \decay{\Bp}{\Kp\pim\pip\mup e^-} decays will use the same control channels as the $R_{\Kp\pim\pip}$ measurement and is planned to proceed in a similar way:
  
  - As previously, a specialized *Stripping* selection will need to be implemented as soon as possible in order to start the study of the control channel using 2016 data.

  - After suitable simulation has been produced, it will be reweighed to match data using the control channel, focusing on occupancy-related variables.

  - A selection will be performed using a \bdt, trained on simulated signal and 5-body invariant mass sideband data as background.
  The possibility of using same-sign $\Kp\pim\pip\mu^\mp e^\mp$ combinations as a proxy for the combinatorial background description will also be studied.

  - A thorough study of possible physics backgrounds will be performed:
  given the low predicted signal yield---even in the case of new physics effects---it is imperative to perfectly know the expectations of each of them in order to obtain a precise measurement.
  Special attention will need to be paid to semileptonic \decay{\Bp}{\Dz(\to\Kp\pim\pip e^+\nu_e)\mup\nu_\mu} and \decay{\Bp}{\Dz(\to\Kp\pim\pip \mup\nu_\mu)e^+\nu_e} decays, with large branching fractions and the same visible final state.

  - Signal shapes will be determined from data using the resonant modes, and a fit model including the previously studied backgrounds will be defined.
  The possibility of performing a simultaneous fit of the 5-body invariant mass in bins of the \bdt output, which can increase the sensitivity \cite{LHCb-PAPER-2013-046}, will also be considered.

Observation of a signal would be an unequivocal sign of new physics.
In absence of observation, a limit to the branching fraction will be set using the CLs method\ \cite{cls}.
Given his or her increasing expertise, the student is expected to take a more preeminent role in this analysis, including the publication procedure, in practice sharing all aspects of the work with me.

### Exploration of inclusive \decay{\B}{X \tau\tau} decays

As previously discussed, \fcnc with taus constitute an excellent laboratory for the study and characterization of new physics effects.
So far, the study of rare tauonic decays at \lhcb has been hindered by the challenges in the reconstruction of the tau lepton, which flies significantly inside the detector and which decays to at least one neutrino.
The last goal of this project is to explore the possibility of improving the tau reconstruction in order to perform an inclusive measurement of \decay{b}{s\tau\tau} decays:
the inclusion of final states such as \decay{\Bz}{\Kp\pim\tau\tau}, \decay{\Bs}{\Kp\Km\tau\tau} or \decay{\Bp}{\Kp\pim\pip\tau\tau} is envisioned to enhance the observed signal and allow a measurement that would otherwise be very difficult.

While only analyses with \decay{\tau}{\mu\nu\nu}, which represents $17.4\%$ of the tau decays, have been published so far, other interesting decay modes can be studied, also with the aim of increasing the signal yield:

  - The \decay{\tau}{\pi\pi\pi\nu} mode---$9\%$ of tau decays---is very interesting because it only contains one neutrino and the kinematics of the process can be solved up to a twofold ambiguity thanks to the decay vertex being known.
  Integration of the reconstruction of this mode into *DecayTreeFitter*---a tool designed to perform a kinematic fit of a full decay chain\ \cite{decaytreefitter}---will be pursued.
  Its current implementation is not designed to handle missing particles, which need to be added and their momentum initialized manually;
  the final result is very sensitive to the initialization value, so a detailed study will be needed to determine which is the best approach.
  In addition, the analytical determination of the tau momenta is possible, up to some ambiguity, in the case where a secondary vertex---the \B decay vertex---is well determined.
  This information could be implemented inside a specialized version of the tool.

  - The \decay{\tau}{h\nu} modes, where $h$ is a kaon or a pion, account for $7\%$ and $10\%$ of the tau decays.
  They represent a great challenge due to the large multiplicity of $K$ and $\pi$ produced in a typical \lhc collision, but very tight isolation requirements and a large displacement of the first measurement in the tracking system could be exploited to reduce the background levels.

  - The \decay{\tau}{e \nu\nu} decay mode represents $17.8\%$ of the tau decays but is very challenging due to the difficulties in electron reconstruction, mainly related to bremsstrahlung radiation.
  A large effort has been put in \lhcb to improve the reconstruction of electrons---crucial in the searches for new physics---so the reconstruction of this mode may benefit from recent and future improvements.

These will be studied, sharing tasks and modes between the student and me, in parallel to the last stages of the previous studies.
With some improvements in the \decay{\tau}{\mu\nu\nu} reconstruction and the addition of some of these extra decay modes it may be possible to achieve a good sensitivity for an inclusive \decay{b}{s\tau\tau} measurement.
A study of simulated events will also be carried out to determine the sensitivity of the measurement, eventually moving to an analysis of the data if feasible.

Schedule and milestones
-----------------------

The project schedule, according to the research plan outlined in the previous section, is displayed in Fig.\ \ref{fig:schedule}.
In a first phase, most work will be focused on the photon polarization measurement, several tasks of which can be developed in parallel.
Additionally, the *Stripping* selections for \decay{\Bp}{\Kp\pim\pip\ellp\ell^{(\prime)-}} will be promptly prepared in order to benefit from the end-of-the-year campaign.
Afterwards, the lepton flavor universality and lepton flavor violation studies will be performed in sequence, parallelizing those tasks common to both analyses and giving increasing autonomy to the student.
Towards the end of the lepton flavor universality analysis, work will commence in the exploration of rare tauonic \B decays, which will be kept in parallel to the main analysis tasks.

\begin{figure}[thb]
\centering
\begin{ganttchart}[hgrid, vgrid,
                   title height=1,y unit title=0.6cm,
                   bar label font=\small,
                   title label font=\bfseries\small,
                   group right shift=0, group left shift=0, group top shift=0.5, group height=0.2, group peaks width={0.1},
                   x unit=0.05\textwidth,
                   y unit chart=0.5cm,
                   inline=false]{1}{12}
% Years
\gantttitle[]{2016}{1}
\gantttitle[]{2017}{4}
\gantttitle[]{2018}{4}
\gantttitle[]{2019}{3} \\
% Quarters
\gantttitle{Q4}{1}
\gantttitle{Q1}{1}\gantttitle{Q2}{1}\gantttitle{Q3}{1}\gantttitle{Q4}{1}
\gantttitle{Q1}{1}\gantttitle{Q2}{1}\gantttitle{Q3}{1}\gantttitle{Q4}{1}
\gantttitle{Q1}{1}\gantttitle{Q2}{1}\gantttitle{Q3}{1} \\
% Polarization
\ganttgroup{Photon polarization}{1}{6} \\
\ganttbar[bar/.append style={pattern=north west lines}]{\minttwo development}{1}{2}\\
\ganttbar[bar/.append style={pattern=north west lines}]{5D model}{3}{3}\\
\ganttbar[bar/.append style={fill=gray}]{New event selection}{1}{2}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Second \lhc run data}{3}{3}\\
\ganttbar[bar/.append style={fill=gray}]{Background + mass fit}{2}{2}\\
\ganttbar{Efficiency simulation}{2}{3}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Data fit and systematics}{4}{4}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Publication}{5}{6}\\
\ganttgroup{Lepton flavor universality}{1}{9}\\
\ganttbar{Stripping}{1}{1}\\
\ganttbar[bar/.append style={fill=gray}]{Event selection}{5}{5}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Background + mass fit}{6}{7}\\
\ganttbar{Efficiency and systematics}{6}{7}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Publication}{8}{9}\\
\ganttgroup{Lepton flavor violation}{1}{11}\\
\ganttbar{Stripping}{1}{1}\\
\ganttbar[bar/.append style={fill=gray}]{Event selection}{5}{5}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Background + mass fit}{8}{9}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Efficiency and systematics}{8}{9}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Publication}{10}{11}\\
\ganttgroup{Tauonic decays exploration}{9}{12}\\
\ganttbar[bar/.append style={pattern=north west lines}]{$\tau$ reconstruction}{9}{12}\\
\ganttbar[bar/.append style={pattern=north west lines}]{Sensitivity study}{12}{12}
\end{ganttchart}
\caption{Project schedule for the student, myself and both as gray, white and striped bars, respectively.\label{fig:schedule}}
\end{figure}


Relevance and impact
--------------------

The measurement of the photon polarization will shed light in one of the few unexplored corners of the \sm and will allow to heavily constrain the size of right-handed currents.
If no significant deviations from the \sm are found, it is expected that---after its foreseen publication in a high-impact peer-reviewed journal and its presentation in high profile 2018 winter conferences---it will be possible to discard a wide array of new physics models with large right-handed currents. 
Additionally, it is planned to present the \minttwo framework in a high energy physics computing conference such as CHEP, with its corresponding proceedings, and to setup a public website with well-prepared, thorough documentation;
this will allow to spread the framework to other members of the collaboration tackling similar analyses and to mutually benefit from their feedback.

The studies on lepton flavor universality will play a large role in the clarification of the anomalies observed by \lhcb on data of the first \lhc run.
It is expected that several similar analyses will be performed within the collaboration, and the combination of all these efforts will allow to paint a clear picture of the subject by the end of the second run of the \lhc.
Therefore, it is planned to publish them in a high-impact peer-reviewed journal and present them in a high profile 2019 winter conference.

If non-\sm physics are confirmed, the search for lepton flavor violating decays will become a hot topic as a way to identify the origin of the new physics phenomena.
The publication of the \decay{\Bp}{\Kp\pim\pip\mup e^-} analysis---also to be presented in a high profile conference in summer 2019---will have an important impact on theoretical models in any of its possible outcomes---observation or non-observation of \lfv---and will help shape the analysis strategies for the third run of the \lhc.

Tauonic decays are also expected to gain relevance at the end of the second \lhc run---especially if lepton flavor nonuniversality is confirmed---and may become one of the key topics of the third run.
It will be very important to stay ahead of the curve and start working to increase efficiencies and to test new techniques to make analyses of rare tauonic decays possible.
In this sense, the outcome of the studies towards \decay{b}{s\tau\tau} measurements will---at least---be documented in an \lhcb note that will serve as reference for future analyses.


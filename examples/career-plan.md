I have been involved in particle physics since 2007.
After completing a \phd at the University of Barcelona and a postdoctoral fellowship at \textsc{epfl}, I was awarded an \snf Ambizione Grant hosted by the University of Zürich, under which I am currently studying rare decays of beauty mesons.
In my career I have participated in projects covering a wide array of subjects, mainly in online and offline computing, detector calibration, and data analysis.
Additionally, I have held several leadership positions in the \lhcb collaboration, increasing in importance as my career progressed.

Within the \lhcb collaboration, I played a major role in the establishment of the rare radiative beauty decays program, both as analyst and coordinator of the working group.
I have also been, for the last two years, in charge of the coordination of the rare decays group, with responsibility over a group of 80 scientists that have produced some of the most relevant results published by \lhcb, including the anomalies observed in measurements of rare semileptonic decays.

It is to these anomalies---one of the most solid hints of physics beyond the Standard Model and one of the most important topics in particle physics nowadays---that I plan to continue devoting most of my efforts in the next few years:
first, using the data collected up to now to try to confirm these anomalies and observe New Physics, and afterwards using the \lhcb upgrade to characterise them.
This observation would represent a revolution in the field, and it is my short- and mid-term goal to use my experience as analyst and coordinator to obtain funding to start my own group and play a leading role in it.
Going further into the future, the confirmation or not of the anomalies will guide the next steps of my career---and of the field:
whether it is pursuing the energy or the intensity frontier, my group will play an important part either in new searches for physics beyond the standard model or in the precise characterisation of new phenomena.

I also aim to continue my involvement in software development on two fronts:
on one hand, pursuing the ideal of veering away from current monolithic software frameworks and building a well maintained ecosystem of interoperable libraries for high energy physics, which would help optimise resources by fostering sharing between experiments, and facilitate data preservation and analysis reproducibility;
on the other, exploring new ways of applying advanced machine learning techniques to tackle the challenges of future high-luminosity experiments.

In the longer term, my goal is to contribute to the development of detectors for these future experiments, putting special emphasis on the possibilities of collaboration with other fields:
new calorimeter technologies, a sector which will require the development of fast, radiation hard detectors and cutting edge machine learning techniques, are specially appealing for their potential synergies with high speed medical imaging.

To achieve these objectives, I am aiming at an academic position in Europe or a research position at \textsc{cern}.
Collaboration with other fields will be a cornerstone of my long term research plan, which I aim to fulfil by participating in meaningful detector \rd projects and by contributing to the open source scientific computing community.
As a teacher, I will prioritise honing problem-solving skills and effective teamwork, fostering classroom interactivity to improve the understanding of course concepts, and providing useful training to make the students valuable in the job market.
I will also take part in outreach activities, putting special focus on the younger generations:
from participating in international Masterclasses to the development of new exhibition material to showcase the work of particle physicists, these tools are of paramount importance to interest young minds in pursuing careers in \textsc{stem}.

As a scientist, there is no better legacy than outstanding research, the expansion of one's field to new roads and a lasting impact on society.

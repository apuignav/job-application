Job application suite
=====================

Set of templates to create a coherent job application suite, including CV, cover letter, research proposal and one-page documents, such as career plan or research statement.

The documents are typeset in [`TeX Gyre Pagella`](http://www.gust.org.pl/projects/e-foundry/tex-gyre/) and [`Fira Sans Light`](https://github.com/mozilla/Fira), with [`Inconsolata`](http://www.levien.com/type/myfonts/inconsolata.html) as monospace font.
The [`Font Awesome`](https://fortawesome.github.io/Font-Awesome/) symbols are also used.

Requirements
------------

    + [pandoc](http://pandoc.org/)
    + [bibtool](https://www.ctan.org/tex-archive/biblio/bibtex/utils/bibtool/?lang=en)
    + Python [bibtexparser](https://pypi.python.org/pypi/bibtexparser/0.6.2) and [pdftools][https://github.com/stlehmann/pdftools]
    + The fonts mentioned before

Document types
--------------

### CV

CV is built based on yaml configuration files
(idea taken from [http://mrzool.cc/tex-boilerplates/](http://mrzool.cc/tex-boilerplates/)).

Need to improve the style based on ideas from:
  
  + [http://practicaltypography.com/resumes.html](http://practicaltypography.com/resumes.html)
  + [https://github.com/posquit0/Awesome-CV](https://github.com/posquit0/Awesome-CV)
  + [http://adavid.web.cern.ch/adavid/cv.pdf](http://adavid.web.cern.ch/adavid/cv.pdf)

### Publication list

Publication list is built from yaml configuration files.
Currently it's divided in "specialized" sections + a full list of publications (LHCb-PAPER + LHCb-DP) at the end.

### Conference list
The conference list is built from yaml configuration files.
It adds the proceedings reference with each contribution.

### Cover letter

Cover letter build from a combination of Markdown and yaml configuration files.
Whenever possible, it takes the information from the common yaml configuration (`personal.yaml`), but this can be overridden.

### Long and short documents

The `templates/template_project.tex` provides a template for multi- and single-page documents.
There is the possibility of having a title page or in-page titles, custom headers for the first page, etc.
Suitable examples can be found in the `examples/` folder.


How to use the templates
------------------------

The templates can be found in the `templates` directory under the name `template_*.tex`.
In all of them, several booleans can be used to fine-tune the behavior of the document (all of them can be set with the YAML key of the same name):

  + `articletitles` (default: `true`)
  + `uprightparticles` (default: `false`)
  + `smallcaps` (default: `true`)
  + `extendedtitles` (default: `false`)
